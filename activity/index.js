console.log("hello world!");


// Part1

function login(){

let username = prompt("Enter your username:");
let password = prompt("Enter your password:");
let role = prompt("Enter your role:").toLowerCase();

	if (username == null || username == ""){
		alert("Input must not be empty."); 
	}
	else if (password == null || password == ""){
		alert("Input must not be empty."); 
	}
	else if (role == null || role == ""){
		alert("Input must not be empty."); 
	}
	else {
		
		switch (role){
			case 'admin':
				alert("Welcome back to the portal, admin!");
				break;
			case 'teacher':
				alert("Thank you for logging in, teacher!");
				break;
			case 'student':
				alert("Welcome back to class, student!");
				break;
			default:
				alert("Role out of range!");
				break;
		}
	}
};
login();


// Part2

function checkAverage(grade1, grade2, grade3, grade4){

	let average = Math.round((grade1 + grade2 + grade3 + grade4)/4);
	console.log(average);
 
	if (average  <= 74){							// 74 and below
		console.log("Hello, student, your average is " + average + ". " + "The letter equivalent is F.");
	}
	else if (average  >= 75 && average <= 79){		// 75 - 79
		console.log("Hello, student, your average is " + average + ". " + "The letter equivalent is D.");
	}
	else if (average  >= 80 && average <= 84){		// 80 - 84
		console.log("Hello, student, your average is " + average + ". " + "The letter equivalent is C.");
	}
	else if (average  >= 85 && average <= 89){		// 85 - 59
		console.log("Hello, student, your average is " + average + ". " + "The letter equivalent is B.");
	}
	else if (average  >= 90 && average <= 95){		// 90 - 95
		console.log("Hello, student, your average is " + average + ". " + "The letter equivalent is A.");	
	} 
	else {  
		console.log("Hello, student, your average is " + average + ". " + "The letter equivalent is A+.");
	}

};
checkAverage(75, 70, 70, 72);
checkAverage(77, 78, 77, 79);
checkAverage(80, 82, 75, 86);
checkAverage(87, 88, 89, 86);
checkAverage(93, 94, 96, 90);
checkAverage(97, 97, 97, 96);

