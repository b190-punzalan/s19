// console.log("hello b190");

// if statement
/*
-executes a statement if a specified condition is true
- can stand alone without the else statement

SYNTAX
	if(condition){
	statement/code block
	}
*/

let numA = -1;
if(numA<0){
	console.log("Hello");
}
console.log(numA<0);

if(numA>0){
	console.log("This statement will not be printed!")
}

let city = "New York"
if(city==="New York"){
	console.log("Welcome to NY City!");
}

// kaso si if pag false, wala naman siyang ibang gagawin/execute. gamitin si else if clause para icheck yung ibang conditions. kung mag-true si else if condition, may iba siyang pwedeng iexecute
// else if clause
/*
-executes a statement if previous conditions are false and if the specified condition is true
- "else if" clause is optional and can be added to capture additional conditions to change the flow of a program. optional lang kasi very independent naman si If clause. magwowork siya kahit wala si else if. si else if naman, very dependent sa if clause. pag gumawa ka ng else if without the if clause, magkaka-error
- walang limit sa kung ilang else if ilalagay, basta may condition ka ibibigay para irun niya pag nag-true
*/

let numB=1;

if (numA > 0){
	console.log("hello")
} else if (numB > 0){
	console.log("world");
}

if (numA < 0){
	console.log("hello")
} else if (numB > 0){
	console.log("world");
}

// pag true na si If clause, hindi na babasahin si else if clause kasi maeexecute na yung condition. bababa lang siya sa else if pag false si if.

city = "Tokyo";
if(city==="New York"){
	console.log("Welcome to NY City")
} else if (city==="Tokyo"){
	console.log("Welcome to Tokyo");
}

// else statement
/*
-executes a statement if all other conditions are false
- the "else" statement is optional and can be added to capture any other result to change the flow of a program
*/

if (numA > 0){
	console.log("hello")
} else if (numB===0){
	console.log("world");
} else {
	console.log("again");
}

// Another example
let age=20;

if (age<=18){
	console.log("Not allowed to drink");
} else {
	console.log("Matanda ka na, shot na!");
}

// Another example
// let age = prompt("Enter your age:");

// if (age<=18){
// 	console.log("Not allowed to drink");
// } else {
// 	console.log("Matanda ka na, shot na!");
// }

// mini-activity

// let height = prompt("Enter height:");

// if (height<150){
// 	console.log("Did not pass minimum requirement");
// } else {
// 	console.log("Passed the minimum requirement.");
// };

// stretched goal: put it inside a function:
function heightRequirement(height){
	// let height = prompt("Enter height:");
		// wala na ito kasi mag-iinvoke ka sa baba mamaya ng heights, otherwise mag-error. 
	
	if (height<150){
	console.log("Did not pass minimum requirement");
	} else {
	console.log("Passed the minimum requirement.");
	}
};

heightRequirement(150);
heightRequirement(100);
heightRequirement(180);

let message = "No message.";
console.log(message);

function determineTyphoonIntensity(windSpeed){

	if (windSpeed < 30){
		return 'Not a typhoon yet.'
	} else if (windSpeed <= 61){
		return 'Tropical depression detected.'
	} else if (windSpeed >= 62 && windSpeed <= 88){
		return 'Tropical storm detected.'
	} else if (windSpeed >= 89 && windSpeed <= 117){
		return 'Severe tropical storm detected.'
	} else {'Typhoon detected.'}
}
message = determineTyphoonIntensity(69)
console.log(message);

// bakit nagre-assign tayo? para magregister sa console or maprint yung message. 

if (message == "Tropical storm detected."){
	console.warn(message)
}
// may warning sign kasi console.warn

//Truthy and Falsy
/*
- In javaScript, a truthy value is a value that is considered true when encountered in a Boolean context
- Values are considered true unless defined otherwise

Falsy values/exceptions for truthy - these are values considered false if we use them in our conditions
1. false - pag false un values, automatic false ang return niya
2. 0
3. -0
4. ""
5. null
6. undefined
7. NaN
*/ 

// Truthy Examples
if(true){
	console.log("Truthy!");
}

if(1){
	console.log("Another truthy");
} 

if([]){
	console.log("truthy");
}

// Falsy examples
if(false){
	console.log("Falsy");
}

if(0){
	console.log("Falsy");
}

if (undefined){
	console.log("Falsy");
}

// walang lumabas sa console kasi false yung mga statements	

// conditional (Ternary) operator
/*
the Ternary operator takes in three operands:
1. condition
2. expression to execute if the condition is truthy
3. expression to execute if the condition is falsy

Syntax:
	(condition) ? ifTrue : ifFalse
*/

// Single Statement execution
let ternaryResult = (1<18) ? "Statement is True" : "Statement is False";
console.log("Result of Ternary Operator: " + ternaryResult)

let name;

/*function isOfLegalAge(){
	name = "John";
	return "You are of the legal age limit";
}

function isUnderAge(){
	name = "Jane";
	return "Your are under the age limit";
}

let age = parseInt(prompt("What is your age?"));
console.log(age);
console.log(typeof age)
let legalAge = (age>18) ? isOfLegalAge() : isUnderAge();
console.log("Result of Ternary Operator in functions: " + legalAge + ", " + name);*/
// ano ang parseInt? It parses a string argument and returns an integer. pag naglagay kasi ng input si user sa prompt, ang return ay nagiging string data type. kaya tayo magparseInt para maging integer yung data type.

// Switch Statement
/*
	Syntax:
		switch (expression) {
			case value : 
				statement
				break;
			default:
				statement;
				break;
		}
*/


let day = prompt("What day of the week is it today?").toLowerCase();
console.log(day);

	switch (day){
		case 'monday':
			console.log("The color of the day is red");
			break;
		case 'tuesday':
			console.log("The color of the day is orange");
			break;
		case 'wednesday':
			console.log("The color of the day is yellow");
			break;
		case 'thursday':
			console.log("The color of the day is green");
			break;
		case 'friday':
			console.log("The color of the day is blue");
			break;
		case 'saturday':
			console.log("The color of the day is indigo");
			break;
		case 'sunday':
			console.log("The color of the day is violet");
			break;
		default:
			console.log("Pelase put a valid day!");
			break;
	}
	// case sensitive po ang data sa switch, kaya gumamit tayo ng toLowerCase function para maging case-insensitive. Otherwise, sobrang dami mong need input na cases depende sa variation na pwedeng ilagay ni user. kaya kugn toLowerCase, iconvert na agad to lower case kahit paano input ni user

	// sa sample, meron na tayong ineexpect na inputs na nilagay sa cases imbes na condition
	// si default acts like an else statement. pag walang nagmatch sa input ni user, ito yung lilitaw/return
	// pwede po bang mag-innput ng dalawang value sa isang case?

	// try-catch-finally statement
	// this is usually used for error-hunting
	function showIntensityAlert(windSpeed){
		try {
			alerat(determineTyphoonIntensity(windSpeed))		
		}
		// sa try statement, attempt lang to execute a code 
		catch (error) {
			console.log(error)
			console.warn(error.message)
		}
		// sa catch naman, it will catch the errors within the try statement
		finally {
			alert("Intensity updates will show new alert.")
		}
		// finally will continue execution of code regardless of success or failure of code execution in the "try" block to handle/resolve errors
	}
	showIntensityAlert(56);